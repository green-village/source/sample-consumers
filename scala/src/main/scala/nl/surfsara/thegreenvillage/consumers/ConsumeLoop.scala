package nl.surfsara.thegreenvillage.consumers

import java.util
import java.util.concurrent.CountDownLatch

import grizzled.slf4j.Logging
import org.apache.kafka.clients.consumer.{CommitFailedException, ConsumerRecord, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException
import org.thegreenvillage.record.GreenVillageRecord

import scala.collection.JavaConverters._

/**
  * Thread for consuming messages from Kafka. This specific consumer is typed to consume Green Village records. See the
  * corresponding schema definitions in src/main/avro.
  *
  * @author Mathijs Kattenberg - mathijs.kattenberg@surfsara.nl
  * @param consumer The consumer to use for consumption
  * @param topics   The topic(s) to consume from
  */
class ConsumeLoop(consumer: KafkaConsumer[String, GreenVillageRecord], topics: util.List[String]) extends Runnable with Logging {
  // Latch for proper shutdown and offset commit during shutdown
  val shutdownLatch = new CountDownLatch(1)

  /**
    * Process a consumed record: Log the data.
    *
    * @param record the consumed record. Note that it is also uses the generated classes from the avro schema.
    */
  def process(record: ConsumerRecord[String, GreenVillageRecord]): Unit = {
    info("Consumed record:")
    info("Record key: " + record.key())
    val gvRecord = record.value()
    info(gvRecord.toString)
  }

  /**
    * Run our thread
    */
  override def run(): Unit = {
    try {
      // Subsribe to the topics
      consumer.subscribe(topics)

      // Poll Kafka forever. The poll blocks until messages are consumed. Offsets are committed after process(record)
      // has returned. Note that the offsets are in sync with the processing if polling and committing offsets this way
      // (commit sync).
      while (true) {
        val records: ConsumerRecords[String, GreenVillageRecord] = consumer.poll(Long.MaxValue)
        records.asScala.foreach(record => process(record))
        doCommitSync()
      }
    } catch {
      case e: WakeupException =>
      // ignore, we're closing
      case e: Exception =>
        logger.error("Unexpected error", e)
    } finally {
      // cleanup
      consumer.close()
      shutdownLatch.countDown()
    }
  }

  /**
    * Shut this consumer down. Called when shutting down the application.
    */
  def shutdown(): Unit = {
    consumer.wakeup()
    try
      shutdownLatch.await()
    catch {
      case e: InterruptedException =>
        error("Interrupted while waiting for latch", e)
    }
  }

  /**
    * Commit the offsets. Ensure they are commited with shutdown.
    */
  private def doCommitSync(): Unit = {
    try
      consumer.commitSync()
    catch {
      case e: WakeupException =>
        // we're shutting down, but finish the commit first and then
        // rethrow the exception so that the main loop can exit
        doCommitSync()
        throw e
      case e: CommitFailedException =>
        // the commit failed with an unrecoverable error. if there is any
        // internal state which depended on the commit, you can clean it
        // up here. otherwise it's reasonable to ignore the error and go on
        logger.debug("Commit failed", e)
    }
  }

}
