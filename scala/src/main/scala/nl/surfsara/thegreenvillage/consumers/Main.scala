package nl.surfsara.thegreenvillage.consumers

import java.io.File
import java.util
import java.util.Properties
import java.util.concurrent.Executors

import com.typesafe.config.{Config, ConfigFactory}
import grizzled.slf4j.Logging
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.ProducerConfig
import org.thegreenvillage.record.GreenVillageRecord

/**
  * Main entrypoint
  *
  * @author Mathijs Kattenberg - mathijs.kattenberg@surfsara.nl
  */
object Main extends Logging {
  // Threadpool for running consumer loop
  val executor = Executors.newCachedThreadPool()

  // Singleton inner object for config
  object ConfigContext {
    var config = ConfigFactory.load()
  }

  // The consumeloop, uninitialized
  var consumeLoop: Option[ConsumeLoop] = None

  /**
    * Main entry point. Starts the consumer
    *
    * @param args Arguments to the program. One arg is optional: a config file to use (see resources/reference.conf)
    */
  def main(args: Array[String]): Unit = {
    // Load and init config
    loadConfig(args)
    val config = ConfigContext.config
    val kafkaConfig = config.getConfig("sampleconsumer.kafkasettings")

    // Read topics to consume from config
    val topicsConsumed: util.List[String] = kafkaConfig.getStringList("topics")

    // Init a consumer. Note that these are typed. The types are generated from avro schemas via a Gradle task. You
    // might need to issue a ./gradlew build for them to generate while editing in your ide.
    val consumer = new KafkaConsumer[String, GreenVillageRecord](initKafkaConsumerProperties(kafkaConfig))

    consumeLoop = Some(new ConsumeLoop(consumer, topicsConsumed))
    consumeLoop match {
      case Some(loop) => {
        // Execute our loop in our threadool
        executor.execute(loop)
      }
      case None => error("Could not initialize consumer loop")
    }

    // Register shutdownhook for proper cleanup
    registerShutdownHook()
  }

  /**
    * Initialize a Properties object from a config file for constructing a consumer
    *
    * @param kafkaConfig The Config to use
    * @return A Properties object with Kafka consumer properties from the config file
    */
  def initKafkaConsumerProperties(kafkaConfig: Config): Properties = {
    val props = new Properties
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getStringList("bootstrap_servers"))
    props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConfig.getString("consumer_group"))
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.KafkaAvroDeserializer")
    props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "true")
    props.put("security.protocol", kafkaConfig.getString("security_protocol"))
    props.put("sasl.kerberos.service.name", "kafka")
    props.put("schema.registry.url", kafkaConfig.getString("schema_registry"))

    props
  }

  /**
    * Load the config, are use reference config (resources/reference.conf)
    *
    * @param args The arguments to the program. The config file to use can be specified as first argument
    */
  def loadConfig(args: Array[String]): Unit = {
    var configFileArg = ""
    if (args.length >= 1) {
      configFileArg = args(0)
    }
    val configFile = new File(configFileArg)
    val configFromFile = ConfigFactory.parseFile(configFile)
    ConfigContext.config = ConfigFactory.load(configFromFile)
    ConfigContext.config.checkValid(ConfigFactory.defaultReference(), "sampleconsumer")
  }

  /**
    * For proper shutdown the threads need to be cleaned up. The consumer should be able to commit the last offsets to
    * Kafka prior to shutting down to prevent reprocessing of messages that have already been processed.
    */
  def registerShutdownHook(): Unit = {
    import java.util.concurrent.TimeUnit
    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        consumeLoop match {
          case Some(l) => l.shutdown()
          case None => None
        }
        executor.shutdown
        try
          executor.awaitTermination(10000, TimeUnit.MILLISECONDS)
        catch {
          case e: InterruptedException =>
            error(e.getMessage)
        }
      }
    })
  }

}
