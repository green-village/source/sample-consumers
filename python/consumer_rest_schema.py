import json
import requests
import logging
import base64


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__file__)

# The Kafka REST Proxy credentials can be passed in the header.
# The format is a base64-encoded username:password.
auth_plain = "demo1-kafka-rest-user:demo1-kafka-rest-password"
auth_base64 = base64.b64encode(auth_plain.encode()).decode("utf-8")

kafka_rest_proxy = "http://gv-demo1-rest.projects.sda.surfsara.nl"
topic = "demo1_schema"
consumer_group = "demo1-group-schema"
consumer_instance = "my_consumer_instance"

# Delete the consumer instance belonging to the consumer group
headers = {
        "Content-Type": "application/vnd.kafka.v2+json",
        "Authorization": "Basic {}".format(auth_base64)
}
url = kafka_rest_proxy + "/consumers/" + consumer_group+ "/instances/" + consumer_instance
r = requests.delete(url, headers=headers)
logger.info(r.text)
logger.info(r.status_code)

# Create a new consumer instance belonging to the consumer group
data = {
        "name": consumer_instance,
        "format": "avro",
        "auto.offset.reset": "latest"
}
headers = {
        "Content-Type": "application/vnd.kafka.v2+json",
        "Authorization": "Basic {}".format(auth_base64)
}
url = kafka_rest_proxy + "/consumers/" + consumer_group
r = requests.post(url, json=data, headers=headers)
logger.info(r.text)
logger.info(r.status_code)

# Subscribe to the topic
data = {"topics": [topic]}
headers = {
        "Content-Type": "application/vnd.kafka.v2+json",
        "Authorization": "Basic {}".format(auth_base64)
}
url = kafka_rest_proxy + "/consumers/" + consumer_group + "/instances/" + consumer_instance + "/subscription"
r = requests.post(url, json=data, headers=headers)
logger.info(r.text)
logger.info(r.status_code)

# Consume messages
while True:
    headers = {
            "Accept": "application/vnd.kafka.avro.v2+json",
            "Authorization": "Basic {}".format(auth_base64)
    }
    url = kafka_rest_proxy + "/consumers/" + consumer_group + "/instances/" + consumer_instance + "/records"
    r = requests.get(url, headers=headers)
    logger.info(r.status_code)
    logger.info(r.text)

    records = json.loads(r.text)
    for record in records:
        print(record)
