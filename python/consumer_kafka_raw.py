from confluent_kafka import KafkaError
from confluent_kafka import Consumer
from pprint import pformat
import json
import logging
import sys


def stats_cb(stats_json_str):
    stats_json = json.loads(stats_json_str)
    print('\nKAFKA Stats: {}\n'.format(pformat(stats_json)))


if __name__ == '__main__':
    # Consumer configuration
    # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
    conf = {
        'bootstrap.servers': ', '.join(['gv-kafka-kafka-{}.gv-kafka-broker.ns-green-village.svc.cluster.local:9092'.format(i) for i in range(7)]),
        #"debug": "security",
        "security.protocol": "SASL_PLAINTEXT",
        'sasl.mechanisms': 'GSSAPI',
        "sasl.kerberos.service.name": "kafka",
        "sasl.kerberos.principal": "base@THEGREENVILLAGE.ORG",
        "sasl.kerberos.keytab": "base.keytab",
        # By default, every project has a single consumer group defined per topic
        # for cosuming messages with consumers (other consumer groups are defined
        # for streaming traffic into the database and storage). In case you intend
        # to write multiple consumers receiving all messages, make sure to use
        # a dedicated consumers group each time.
        'group.id': 'base-group-raw',
        'session.timeout.ms': 6000,
        'auto.offset.reset': 'earliest',
        'stats_cb': stats_cb,
        'statistics.interval.ms': 60000}

    # Create logger for consumer (logs will be emitted when poll() is called)
    logger = logging.getLogger('consumer')
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(asctime)-15s %(levelname)-8s %(message)s'))
    logger.addHandler(handler)

    # Create Consumer instance
    # Hint: try debug='fetch' to generate some log messages
    c = Consumer(conf, logger=logger)

    def print_assignment(consumer, partitions):
        print('Assignment:', partitions)

    # Subscribe to topics
    c.subscribe(['base_raw'], on_assign=print_assignment)

    # Read messages from Kafka, print to stdout
    try:
        while True:
            msg = c.poll(timeout=1.0)
            if msg is None:
                continue
            if msg.error():
                # Error or event
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    sys.stderr.write('%% %s [%d] reached end at offset %d\n' %
                                     (msg.topic(), msg.partition(), msg.offset()))
                else:
                    # Error
                    raise KafkaException(msg.error())
            else:
                # Proper message
                sys.stderr.write('%% %s [%d] at offset %d with key %s:\n' %
                                 (msg.topic(), msg.partition(), msg.offset(),
                                  str(msg.key())))
                print(msg.value())

    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')

    finally:
        # Close down consumer to commit final offsets.
        c.close()
