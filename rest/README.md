# Consuming Messages with Kafka REST Proxy

This section shows how to consume messages using Kafka REST Proxy.

https://docs.confluent.io/current/kafka-rest/docs/intro.html

For each project, Kafka REST Proxy is configured with the Kerberos keytab belonging to the project. In this way, only the topics from the project can be accessed.
The examples are given using `curl` from the command line.

With the command below, you will be able to see topics from all projects. However, you can only publish/subscribe to the topics for which you have the access rights.

```
curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics \
    -u base-kafka-rest-user:1234 | jq .
```

Alternatively, the credentials can be passed in the authorization header in a base64-encoded format consisting of username:password.

```
printf '%s' "base-kafka-rest-user:1234" | base64
```

```
curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="
```

Show particular topics.

```
curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics/base_raw \
    -u base-kafka-rest-user:1234 | jq .

curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics/base_schema \
    -u base-kafka-rest-user:1234 | jq .
```



## Raw messages

Consume the `base_raw` topic belonging to the `base-group-raw` consumer group.

Create a consumer for **JSON** data, starting at the beginning of the topic's
log and subscribe to a topic. Then consume some data using the base URL in the first response.
Finally, close the consumer with a DELETE to make it leave the group and clean up
its resources.

```
curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" \
    --data '{"name": "my_consumer_instance", "format": "json", "auto.offset.reset": "latest"}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" --data '{"topics":["base_raw"]}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance/subscription \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="

curl -X GET -H "Accept: application/vnd.kafka.json.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance/records \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X DELETE -H "Content-Type: application/vnd.kafka.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="
```

Create a consumer for **binary** data, starting at the beginning of the topic's
log. Then consume some data from a topic using the base URL in the first response.
Finally, close the consumer with a DELETE to make it leave the group and clean up
its resources.

```
curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" \
    --data '{"name": "my_consumer_instance", "format": "binary", "auto.offset.reset": "latest"}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" --data '{"topics":["base_raw"]}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance/subscription \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="

curl -X GET -H "Accept: application/vnd.kafka.binary.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance/records \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X DELETE -H "Content-Type: application/vnd.kafka.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-raw/instances/my_consumer_instance \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="
```



## Messages in the Avro schema

Consume the `base_schema` topic belonging to the `base-group-schema` consumer group.

Create a consumer for **Avro** data, starting at the beginning of the topic's
log and subscribe to a topic. Then consume some data from a topic, which is decoded, translated to
JSON, and included in the response. The schema used for deserialization is
fetched automatically from schema registry. Finally, clean up.

```
curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" \
    --data '{"name": "my_consumer_instance", "format": "avro", "auto.offset.reset": "latest"}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-schema \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X POST -H "Content-Type: application/vnd.kafka.v2+json" --data '{"topics":["base_schema"]}' \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-schema/instances/my_consumer_instance/subscription \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="

curl -X GET -H "Accept: application/vnd.kafka.avro.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-schema/instances/my_consumer_instance/records \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .

curl -X DELETE -H "Content-Type: application/vnd.kafka.v2+json" \
    https://gv-base-rest.projects.sda.surfsara.nl/consumers/base-group-schema/instances/my_consumer_instance \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA=="
```
